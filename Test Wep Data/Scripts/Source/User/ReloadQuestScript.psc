Scriptname ReloadQuestScript extends Quest

Perk Property ReloadPerk Auto Const

Event OnInit()
    game.GetPlayer().AddPerk(ReloadPerk, false)
    debug.Notification("Adding Perk")
EndEvent
