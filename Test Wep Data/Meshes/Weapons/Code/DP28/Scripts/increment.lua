﻿--Increment the angle of the magazine
function IncrementAngle()

	eventName = hkbGetHandleEventName()
		
	if(eventName == "ReloadComplete") then
		hkbSetVariable("MagRotationVariable", 0.0)
	elseif(eventName == "WeaponFire") then
		degree = hkbGetVariable("MagRotationVariable")
		degree = degree + 7.66
		hkbSetVariable("MagRotationVariable", degree)
	end
end
